package com.dimoll.simplechat.model

data class ChatChannel(val usersIds: MutableList<String>) {
    constructor() : this(mutableListOf())
}