package com.dimoll.simplechat

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.dimoll.simplechat.fragment.PeopleFragment
import com.dimoll.simplechat.fragment.ProfileFragment
import com.dimoll.simplechat.service.MyFirebaseMessagingService
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.activity_main.*
import com.google.firebase.iid.InstanceIdResult
import com.google.android.gms.tasks.OnSuccessListener


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        startService(Intent(this, MyFirebaseMessagingService::class.java))
        replaceFragment(PeopleFragment())
        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
            val deviceToken = instanceIdResult.token
            MyFirebaseMessagingService.addTokenToFirestore(deviceToken)
        }

        nav_view.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.navigation_people -> {
                    replaceFragment(PeopleFragment())
                    true
                }
                R.id.navigation_profile -> {
                    replaceFragment(ProfileFragment())
                    true
                }
                else -> false
            }
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
            .replace(R.id.fragment_layout, fragment)
            .commit()

    }

}
