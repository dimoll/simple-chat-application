package com.dimoll.simplechat.service

import android.util.Log
import com.dimoll.simplechat.util.FireStoreUtil
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {

    override fun onCreate() {
        super.onCreate()
        Log.d("FCM", "CRWATE FUNCTION ON MSG RECEIVED")

    }
    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        remoteMessage?.let {
            Log.d("FCM", it.data.toString())
            Log.d("FCM", "MESSAGE WORK!!!!!!!!!!!!!!!!!!!!")
        }
        Log.d("FCM", "MESSAGE FUNCTION ON MSG RECEIVED")

    }

    override fun onNewToken(p0: String?) {
        Log.d("FCM", "TOKENp0: $p0")
        val registrationToken = FirebaseInstanceId.getInstance().token
        Log.d("FCM", "TOKEN: $registrationToken")
        FirebaseAuth.getInstance().currentUser?.let {
            addTokenToFirestore(registrationToken)
        }
    }

    companion object {
        fun addTokenToFirestore(newRegistrationToken: String?) {
            if (newRegistrationToken == null) throw NullPointerException("FCM token is null")

            FireStoreUtil.getFCMRegistrationTokens { tokens ->
                if (tokens.contains(newRegistrationToken))
                    return@getFCMRegistrationTokens

                tokens.add(newRegistrationToken)
                FireStoreUtil.setFCMRegistrationTokens(tokens)
            }
        }
    }
}