package com.dimoll.simplechat.recyclerview.item

import android.content.Context
import com.dimoll.simplechat.R
import com.dimoll.simplechat.glide.GlideApp
import com.dimoll.simplechat.model.User
import com.dimoll.simplechat.util.StorageUtil
import com.xwray.groupie.kotlinandroidextensions.Item
import com.xwray.groupie.kotlinandroidextensions.ViewHolder
import kotlinx.android.synthetic.main.item_person.*

class PersonItem(
    var person: User,
    val userId: String,
    private val context: Context
) : Item() {
    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.textView_name.text = person.name
        viewHolder.textView_bio.text = person.bio

        person.profilePicturePath?.let {
            GlideApp.with(context)
                .load(StorageUtil.pathToReference(it))
                .placeholder(R.drawable.ic_account_circle)
                .into(viewHolder.imageView_profile_picture)
        }
    }

    override fun getLayout(): Int = R.layout.item_person
}